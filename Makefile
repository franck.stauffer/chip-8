CPPFLAGS = -D_DEFAULT_SOURCE -D_FOTIFY_SOURCE=2
CFLAGS   = $(CPPFLAGS) -std=c99 -pedantic -Wall -Wextra -mtune=native -march=native -O2 -fstack-protector -flto -fPIE -pipe
LDFLAGS  = -Wl,-z,relro,-z,now,-O2,--strip-all -lSDL2 -lSDL2_mixer -flto -pie

default: bin obj bin/chip8

bin:
	mkdir bin

obj:
	mkdir obj

clean:
	rm -rf bin obj

bin/chip8: obj/main.o
	$(CC) -o $@ $^ $(LDFLAGS)

obj/%.o: src/%.c
	$(CC) $(CFLAGS) -c -o $@ $^

format:
	clang-format -i -style="{BasedOnStyle: mozilla, IndentWidth: 4}" src/main.c

.PHONY: test
test: default
	@./bin/chip8 tests/TEST

