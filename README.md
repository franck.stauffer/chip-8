![logo](logo.png)

## Compile-time options

You can tweak some variables:
- `SCALE`: Increase the size of pixels
- `WAIT`: Time waited between two opcodes in microseconds

You can set them in the CPPFLAGS of the Makefile with `-DWAIT=100 -DSCALE=20`

## Dependencies
`SDL2` and `SDL2_Mixer`

## How-to run

```
make
./bin/chip8 ROM
```

## Find ROMs

You can find ROMs [here](https://github.com/dmatlack/chip8/tree/master/roms).

## Test ROM

https://github.com/corax89/chip8-test-rom
